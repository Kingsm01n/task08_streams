package methodsForStreams;

import java.util.ArrayList;
import java.util.Random;

public class FewMethods {
    public static ArrayList<Integer> method1(){
        ArrayList<Integer> arr1 = new ArrayList<>();
        for(int i = 0; i < 50; i++){
            arr1.add((int) (i * Math.random()));
        }
        return arr1;
    }
}
