package FuncInterfaces;

public interface Average {
    public int average(int a, int b, int c);
}
