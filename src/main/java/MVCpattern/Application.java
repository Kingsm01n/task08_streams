package MVCpattern;

import static MVCpattern.Controller.*;

public class Application {
    public static void main(String[] args) {
        lambdaTest();
        streamsTest();
        application();
    }
}
