package MVCpattern;

import FuncInterfaces.Average;
import FuncInterfaces.Max;
import methodsForStreams.FewMethods;

import java.util.*;
import java.util.stream.Collectors;

public class Controller{
    static Scanner sc = new Scanner(System.in);
    public static void lambdaTest(){
        Max max = (a,b,c) -> {return (a+b+c)/3;};
        View.display(max.max(1,2,3));
        Average av = (a,b,c) -> {return c;};
        View.display(av.average(1,2,3));
    }
    public static void streamsTest(){
        List<Integer> list1 = FewMethods.method1();
        long count = list1.stream().count();
    }
    public static void application(){
        int i = 0;
        List<String> arr = new ArrayList<String>();
        do{
             arr.add(sc.nextLine());
             i++;
        }while(!(arr.get(i-1).isEmpty()));
        arr.stream();
        List<String> unique = arr.stream().distinct().collect(Collectors.toList());
        View.display(unique);
        long count = arr.stream().distinct().count();
        View.display(count);
    }
}
